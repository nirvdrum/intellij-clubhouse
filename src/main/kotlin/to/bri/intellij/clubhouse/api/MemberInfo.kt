package to.bri.intellij.clubhouse.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class MemberInfo(

    @JsonProperty("id")
    val id: String,

    @JsonProperty("mention_name")
    val mentionName: String,

    @JsonProperty("name")
    val name: String

)
