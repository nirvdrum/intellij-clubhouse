package to.bri.intellij.clubhouse.api

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class Comment(

    @JsonProperty("id")
    val id: Int,

    @JsonProperty("app_url")
    val url: String,

    @JsonProperty("text")
    val text: String,

    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val createdAt: Date

)
