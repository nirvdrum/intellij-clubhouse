package to.bri.intellij.clubhouse.api

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class Story(

    @JsonProperty("id")
    val id: Int,

    @JsonProperty("app_url")
    val url: String,

    @JsonProperty("name")
    val name: String,

    @JsonProperty("story_type")
    val type: String,

    @JsonProperty("description")
    val description: String,

    @JsonProperty("completed")
    val completed: Boolean,

    @JsonProperty("comments")
    val comments: List<Comment>,

    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val createdAt: Date,

    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    val updatedAt: Date? = null

)
