package to.bri.intellij.clubhouse.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class StorySearchResults(

    @JsonProperty("cursors")
    val cursors: List<String>?,

    @JsonProperty("data")
    val stories: List<Story>,

    @JsonProperty("next")
    val next: String?,

    @JsonProperty("total")
    val total: Int

)
