package to.bri.intellij.clubhouse

import com.intellij.openapi.project.Project
import com.intellij.tasks.impl.BaseRepositoryType
import com.intellij.util.Consumer
import javax.swing.Icon

class ClubhouseRepositoryType : BaseRepositoryType<ClubhouseRepository>() {

    override fun getName(): String {
        return "Clubhouse"
    }

    override fun getIcon(): Icon {
        return ClubhouseIcons.Clubhouse
    }

    override fun createRepository(): ClubhouseRepository {
        return ClubhouseRepository(this)
    }

    override fun createEditor(
        repository: ClubhouseRepository,
        project: Project,
        changeListener: Consumer<ClubhouseRepository>
    ): ClubhouseRepositoryEditor {
        return ClubhouseRepositoryEditor(project, repository, changeListener)
    }

    override fun getRepositoryClass(): Class<ClubhouseRepository> {
        return ClubhouseRepository::class.java
    }

}
