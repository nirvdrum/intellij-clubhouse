package to.bri.intellij.clubhouse

import com.intellij.tasks.Task
import com.intellij.tasks.TaskRepository
import com.intellij.tasks.TaskType
import to.bri.intellij.clubhouse.api.Story
import java.util.*
import javax.swing.Icon

class ClubhouseTask(private val story: Story, private val repository: ClubhouseRepository?) : Task() {

    private val comments: Array<com.intellij.tasks.Comment> = story.comments
        .map(::ClubhouseComment)
        .toTypedArray()

    override fun getId(): String {
        return story.id.toString()
    }

    override fun getSummary(): String {
        return story.name
    }

    override fun getDescription(): String? {
        return story.description
    }

    override fun getComments(): Array<com.intellij.tasks.Comment> {
        return comments
    }

    override fun getIcon(): Icon {
        return when (story.type) {
            "chore" -> ClubhouseIcons.Chore
            "feature" -> ClubhouseIcons.Feature
            else -> ClubhouseIcons.Bug
        }
    }

    override fun getType(): TaskType {
        return when (story.type) {
            "bug" -> TaskType.BUG
            "feature" -> TaskType.FEATURE
            else -> TaskType.OTHER
        }
    }

    override fun getUpdated(): Date? {
        return story.updatedAt
    }

    override fun getCreated(): Date {
        return story.createdAt
    }

    override fun isClosed(): Boolean {
        return story.completed
    }

    override fun isIssue(): Boolean {
        return true
    }

    override fun getIssueUrl(): String {
        return story.url
    }

    override fun getRepository(): TaskRepository? {
        return repository
    }

}
